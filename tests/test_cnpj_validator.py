import pytest
from utils.cnpj_validator_util import CnpjValidator

valid_cnpjs_as_params = pytest.mark.parametrize('cnpj', [
    ('58577114000189'),
    ('11222333000181'),
    ('00038174000143'),  # UnB
])

invalid_cnpj_decimal = pytest.mark.parametrize('cnpj',[
    ('123456789.12345'),
])

invalid_cnpj_decimal_or_wrong_length = pytest.mark.parametrize('cnpj', [
    ('123456789.123'),
    (''),
    ('1234567'),
    ('12345678901234567'),
])

valid_cnpj_decimal_or_wrong_length = [
    ('00038174000143', True),
    ('', False),
    ('1234567', False),
    ('12345678901234567', False),

]

valid_cnpj_decimal_or_length_equal_14 = pytest.mark.parametrize('cnpj',[
    ('11222333000181'),
    ('00038174000143'),
])


invalid_cnpjs_as_params = pytest.mark.parametrize('cnpj', [
    ('11111111111111'),
])

wrong_length_or_non_numeric_cnpjs_as_params = pytest.mark.parametrize('cnpj', [
    # wrong length
    (''),
    ('1234567890123'),
    ('123456789012345'),
    # non numeric
    ('0003817400014_'),
    ('F0038174000143'),
    ('00!38174000143'),
])
sut = CnpjValidator.validate

@valid_cnpjs_as_params
def test_accepts_valid_cnpj(cnpj: str):
    sut(cnpj)

@wrong_length_or_non_numeric_cnpjs_as_params
def test_rejects_cnpj_with_non_numeric_digits(cnpj: str):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'must contain exactly 14 numerical digits' in str(e.value)

@invalid_cnpjs_as_params
def test_rejects_invalid_cnpjs(cnpj: str):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'Invalid' in str(e.value)

@invalid_cnpj_decimal
def test_rejects_invalid_cnpj_decimal(cnpj: float):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'must contain exactly 14 numerical digits' in str(e.value)

@invalid_cnpj_decimal_or_wrong_length
def test_rejects_invalid_cnpj_decimal_or_wrong_length(cnpj: float):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'must contain exactly 14 numerical digits' in str(e.value)

@pytest.mark.parametrize('cnpj, expected_result', valid_cnpj_decimal_or_wrong_length)
def test_valid_cnpj_decimal_or_wrong_length(cnpj: str,  expected_result: bool):
    if expected_result:
        assert sut(cnpj) is None
    else:
        with pytest.raises(Exception) as e:
            sut(cnpj)
        assert str(e.value) == 'CNPJ must contain exactly 14 numerical digits'

@valid_cnpj_decimal_or_length_equal_14
def test_accepts_valid_cnpj_or_length_equal_14 (cnpj: str):
    sut(cnpj)
